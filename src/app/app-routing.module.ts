import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { AuthGuard } from './auth.guard';
const routes: Routes = [
  {path: '', component:HomeComponent},
  {path: 'footer', component:FooterComponent},
  {path: 'header', component:HeaderComponent},
  {path: 'admin',
  loadChildren: ()=>import('./modules/application/management/admin/admin.module').then(mod=>mod.AdminModule),
  canActivate:[AuthGuard]},
  {path: 'attendersevent',
  loadChildren: ()=>import('./modules/application/management/attendersevent/attendersevent.module').then(mod=>mod.AttenderseventModule),
  canActivate:[AuthGuard]},
  {path: 'attendersmatchs',
  loadChildren: ()=>import('./modules/application/management/attendersmatchs/attendersmatchs.module').then(mod=>mod.AttendersmatchsModule),
  canActivate:[AuthGuard]},
  {path: 'calendarsevent',
  loadChildren: ()=>import('./modules/application/management/calendarsevent/calendarsevent.module').then(mod=>mod.CalendarseventModule)},
  {path: 'history',
  loadChildren: ()=>import('./modules/application/userpages/history/history.module').then(mod=>mod.HistoryModule),
  canActivate:[AuthGuard]},
  {path: 'login',
  loadChildren: ()=>import('./modules/application/userpages/login/login.module').then(mod=>mod.LoginModule)},
  {path: 'register',
  loadChildren: ()=>import('./modules/application/userpages/register/register.module').then(mod=>mod.RegisterModule)},

  {path: 'listallevents',
  loadChildren: ()=>import('./modules/application/userpages/listallevents/listallevents.module').then(mod=>mod.ListalleventsModule)},
  {path: 'maps',
  loadChildren: ()=>import('./modules/application/userpages/maps/maps.module').then(mod=>mod.MapsModule),
  canActivate:[AuthGuard]},
  {path: 'matchs',
  loadChildren: ()=>import('./modules/application/management/matchs/matchs.module').then(mod=>mod.MatchsModule),
  canActivate:[AuthGuard]},
  {path: 'members',
  loadChildren: ()=>import('./modules/application/management/members/members.module').then(mod=>mod.MembersModule),
  canActivate:[AuthGuard]},
  {path: 'season',
  loadChildren: ()=>import('./modules/application/userpages/season/season.module').then(mod=>mod.SeasonModule)},
  {path: 'training',
  loadChildren: ()=>import('./modules/application/userpages/training/training.module').then(mod=>mod.TrainingModule),
  canActivate:[AuthGuard]},
  {path: 'inscription',
  loadChildren: ()=>import('./modules/application/userpages/register/register.module').then(mod=>mod.RegisterModule)},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
