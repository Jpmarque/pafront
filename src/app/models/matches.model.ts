export class Match{

  eventId: number;
  pointScored: number;
    pointTaken: number;
    bonusPoint: number;
    effectiveStartDate: Date;
    team: string;


  constructor(eventId: number, pointScored: number, pointTaken: number, bonusPoint: number, effectiveStartDate: Date, team: string) {
    this.eventId = eventId;
    this.pointScored = pointScored;
    this.pointTaken = pointTaken;
    this.bonusPoint = bonusPoint;
    this.effectiveStartDate = effectiveStartDate;
    this.team = team;
  }
}
