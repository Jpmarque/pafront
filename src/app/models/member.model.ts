export class Member {

  roles: any;
  mail: string;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  birthdate: Date;
  validationDate: Date;
  refusalDate: Date;
  adress: string;
  gamesPlayedThisSeason: number;
  dateMedicalCertificate: Date;
  thisYearLicenceDate: Date


  constructor(roles: any, mail: string, firstName: string, lastName: string, phoneNumber: string, birthdate: Date, validationDate: Date, refusalDate: Date, adress: string, gamesPlayedThisSeason: number, dateMedicalCertificate: Date, thisYearLicenceDate: Date) {
    this.roles = roles;
    this.mail = mail;
    this.firstName = firstName;
    this.lastName = lastName;
    this.phoneNumber = phoneNumber;
    this.birthdate = birthdate;
    this.validationDate = validationDate;
    this.refusalDate = refusalDate;
    this.adress = adress;
    this.gamesPlayedThisSeason = gamesPlayedThisSeason;
    this.dateMedicalCertificate = dateMedicalCertificate;
    this.thisYearLicenceDate = thisYearLicenceDate;
  }
}
