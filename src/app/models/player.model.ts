export class Player{
  firstName: string;
  lastName: string;
  userId: number;

  constructor(userId: number, firstName: string, lastName: string) {
    this.userId = userId;
    this.firstName = firstName;
    this.lastName = lastName;

  }
}
