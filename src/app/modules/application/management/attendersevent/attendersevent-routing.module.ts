import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AttenderseventComponent } from './attendersevent.component';

const routes: Routes = [
  {path:'', component:AttenderseventComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AttenderseventRoutingModule { }
