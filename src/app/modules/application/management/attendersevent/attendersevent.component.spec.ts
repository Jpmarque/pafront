import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AttenderseventComponent } from './attendersevent.component';

describe('AttenderseventComponent', () => {
  let component: AttenderseventComponent;
  let fixture: ComponentFixture<AttenderseventComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AttenderseventComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AttenderseventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
