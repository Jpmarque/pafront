import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AttenderseventRoutingModule } from './attendersevent-routing.module';
import { AttenderseventComponent } from './attendersevent.component';


@NgModule({
  declarations: [
    AttenderseventComponent
  ],
  imports: [
    CommonModule,
    AttenderseventRoutingModule
  ]
})
export class AttenderseventModule { }
