import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AttendersmatchsComponent } from './attendersmatchs.component';

const routes: Routes = [
  {path:'', component:AttendersmatchsComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AttendersmatchsRoutingModule { }
