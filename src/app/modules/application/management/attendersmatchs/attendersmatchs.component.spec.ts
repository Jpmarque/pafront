import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AttendersmatchsComponent } from './attendersmatchs.component';

describe('AttendersmatchsComponent', () => {
  let component: AttendersmatchsComponent;
  let fixture: ComponentFixture<AttendersmatchsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AttendersmatchsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AttendersmatchsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
