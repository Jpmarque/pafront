import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AttendersmatchsRoutingModule } from './attendersmatchs-routing.module';
import { AttendersmatchsComponent } from './attendersmatchs.component';


@NgModule({
  declarations: [
    AttendersmatchsComponent
  ],
  imports: [
    CommonModule,
    AttendersmatchsRoutingModule
  ]
})
export class AttendersmatchsModule { }
