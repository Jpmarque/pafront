import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CalendarseventComponent } from './calendarsevent.component';

const routes: Routes = [
  {path:'', component: CalendarseventComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CalendarseventRoutingModule { }
