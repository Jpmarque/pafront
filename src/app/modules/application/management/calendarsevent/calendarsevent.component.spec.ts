import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CalendarseventComponent } from './calendarsevent.component';

describe('CalendarseventComponent', () => {
  let component: CalendarseventComponent;
  let fixture: ComponentFixture<CalendarseventComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CalendarseventComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CalendarseventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
