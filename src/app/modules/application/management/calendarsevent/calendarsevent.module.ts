import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CalendarseventRoutingModule } from './calendarsevent-routing.module';
import { CalendarseventComponent } from './calendarsevent.component';


@NgModule({
  declarations: [
    CalendarseventComponent
  ],
  imports: [
    CommonModule,
    CalendarseventRoutingModule
  ]
})
export class CalendarseventModule { }
