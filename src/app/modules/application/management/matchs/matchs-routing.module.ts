import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatchsComponent } from './matchs.component';

const routes: Routes = [
  {path:'', component: MatchsComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MatchsRoutingModule { }
