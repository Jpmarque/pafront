import {Component, OnInit} from '@angular/core';
import {Match} from "../../../../models/matches.model";
import {MatchesService} from "../../../../service/matches.service";
import {PlayersService} from "../../../../service/players.service";
import {Player} from "../../../../models/player.model";

@Component({
  selector: 'app-matchs',
  templateUrl: './matchs.component.html',
  styleUrls: ['./matchs.component.css']
})
export class MatchsComponent implements OnInit{

  listMatches: Match[];
  listPlayers: Player[];
  displayStyle: any;
  selectedMatch: Match;
  clicked: number;

  constructor(private matchesService: MatchesService, private playersService: PlayersService) {
  }

  ngOnInit(): void {
    this.matchesService.getAllMatches().subscribe( (data: Match[]) => {
      this.listMatches = data;
    })

    this.playersService.getAllPlayers().subscribe((data: Player[])=> {
      this.listPlayers = data;
    })

  }
  accept(player: Player) {
    return this.matchesService.validatePlayer(player.userId, this.selectedMatch.eventId).subscribe((data: any) => {console.log("test accept members");});
  }

  openPopup(match: Match) {
    this.displayStyle = "block";
    this.selectedMatch = match;
    console.log(match);
  }

  closePopup() {
    this.displayStyle = "none";
  }
}
