import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatchsRoutingModule } from './matchs-routing.module';
import { MatchsComponent } from './matchs.component';


@NgModule({
  declarations: [
    MatchsComponent
  ],
  imports: [
    CommonModule,
    MatchsRoutingModule
  ]
})
export class MatchsModule { }
