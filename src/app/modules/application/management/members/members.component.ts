import {Component, OnChanges, OnInit} from '@angular/core';
import {Member} from "../../../../models/member.model";
import {MembersService} from "../../../../service/members.service";

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.css']
})
export class MembersComponent implements OnInit{

  listMembers: Member[];
  public i: number = 0;

  constructor(private membersService: MembersService) {
  }

  ngOnInit(): void {
    this.membersService.getAllMembers().subscribe( (data: Member[]) => {
      this.listMembers = data;
    })
  }

  accept(member: Member) {
    return this.membersService.acceptMember(member.mail)
      .subscribe((data: any) => {console.log("test accept members");this.ngOnInit();});
  }

  refuse(member: Member) {
   return this.membersService.refuseMember(member.mail).subscribe((data: any) => {console.log("test refuse members"); this.ngOnInit();});
  }

  showingOnlyOnce(i: number): number {
  i++;
    return i;
  }
}
