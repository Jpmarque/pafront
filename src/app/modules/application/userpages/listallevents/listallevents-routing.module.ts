import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListalleventsComponent } from './listallevents.component';

const routes: Routes = [
  {path:'', component:ListalleventsComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListalleventsRoutingModule { }
