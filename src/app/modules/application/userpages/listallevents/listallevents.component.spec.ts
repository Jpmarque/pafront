import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListalleventsComponent } from './listallevents.component';

describe('ListalleventsComponent', () => {
  let component: ListalleventsComponent;
  let fixture: ComponentFixture<ListalleventsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListalleventsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListalleventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
