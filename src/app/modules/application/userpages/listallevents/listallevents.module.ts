import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListalleventsRoutingModule } from './listallevents-routing.module';
import { ListalleventsComponent } from './listallevents.component';


@NgModule({
  declarations: [
    ListalleventsComponent
  ],
  imports: [
    CommonModule,
    ListalleventsRoutingModule
  ]
})
export class ListalleventsModule { }
