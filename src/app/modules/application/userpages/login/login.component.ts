import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/service/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit{

  username: string = '';
  password: string = '';
  errorMessage: string = '';
  // role: string = '';

  constructor(private _auth: AuthenticationService, private router: Router){}
  ngOnInit(): void {
  }

  loginUser(){
    this._auth.loginUser(this.username, this.password).subscribe((data=>{
      sessionStorage.setItem("token", data.token);
      alert("Connecté");
      })
    );
  }
}
