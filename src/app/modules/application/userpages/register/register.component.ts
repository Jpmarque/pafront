import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/service/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  username: string = '';
  password: string = '';
  firstname: string = '';
  lastname: string = '';
  phonenumber: string = '';
  birthdate: Date ;
  errorMessage: string = '';

  constructor(private _auth: AuthenticationService,
    private _router: Router){}

  ngOnInit(): void {
      }

  registerUser(){
    this._auth.registerUser(this.username, this.password,this.birthdate,this.firstname,this.lastname,this.phonenumber).subscribe((data =>{
    sessionStorage.setItem("token",data.token);
    alert(data.token);
    this._router.navigate(['']);
    }))
  }
}
