import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SeasonRoutingModule } from './season-routing.module';
import { SeasonComponent } from './season.component';


@NgModule({
  declarations: [
    SeasonComponent
  ],
  imports: [
    CommonModule,
    SeasonRoutingModule
  ]
})
export class SeasonModule { }
