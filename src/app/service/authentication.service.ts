import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {environment} from "../../environments/environment";
const BACKEND_URL = environment.backendUrl;

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private _registerUrl = BACKEND_URL + "/register";
  private _loginUrl = BACKEND_URL + "/authorize";
  private httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };
  constructor(private http: HttpClient) { }

  registerUser(username: string, password: string, birthdate: Date, firstName: string, lastName: string, phoneNumber: string): Observable<any>{
    return this.http.post<any>(this._registerUrl, {username,password, birthdate, firstName, lastName, phoneNumber}, this.httpOptions)
  }
  loginUser(username: string, password: string): Observable<any>{
  return this.http.post<any>(this._loginUrl, {username,password}, this.httpOptions)
}
loggedIn(){
  return !!sessionStorage.getItem('token')
}



getToken(){
  return sessionStorage.getItem('token')
}
}
