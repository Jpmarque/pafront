import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Match} from "../models/matches.model";
import {environment} from "../../environments/environment";
const BACKEND_URL = environment.backendUrl;

@Injectable({
  providedIn: 'root'
})
export class MatchesService{

  private _matchesUrl = BACKEND_URL + "/matches";

  private httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };
  constructor(private http:HttpClient) { }

  public getAllMatches(): Observable<Match[]>{
    return this.http.get<Match[]>(this._matchesUrl);
  }

  validatePlayer(userId: number, eventId: number): Observable<any>{
    const data = {"userId": userId, "eventId": eventId};
    console.log(data);
    return this.http.post(this._matchesUrl + "/validate", data);
  }

}
