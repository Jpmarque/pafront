import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Member} from "../models/member.model";
import {environment} from "../../environments/environment";
const BACKEND_URL = environment.backendUrl;

@Injectable({
  providedIn: 'root'
})
export class MembersService {

  private _membersUrl =  BACKEND_URL + "/members";
  private httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };
  constructor(private http:HttpClient) { }


  public getAllMembers(): Observable<Member[]> {
    return this.http.get<Member[]>(this._membersUrl);
  }

  acceptMember(mail: string): Observable<any> {
    return this.http.put(this._membersUrl + "/accept", JSON.stringify(mail), this.httpOptions);
  }

  refuseMember(mail: string): Observable<any> {
    return this.http.put(this._membersUrl + "/refuse", JSON.stringify(mail), this.httpOptions);
  }
}
