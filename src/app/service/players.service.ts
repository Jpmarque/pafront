import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Player} from "../models/player.model";
import {environment} from "../../environments/environment";
const BACKEND_URL = environment.backendUrl;

@Injectable({
  providedIn: 'root'
})
export class PlayersService{

  private _playersUrl = BACKEND_URL + "/matches/players";
  private httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };
  constructor(private http:HttpClient) { }

  public getAllPlayers(): Observable<Player[]>{
    return this.http.get<Player[]>(this._playersUrl);
  }

}
